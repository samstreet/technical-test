<?php

declare(strict_types=1);

namespace App;

use App\Core\Providers as CoreProviders;
use App\Matches\Providers as MatchesProviders;
use App\Teams\Providers as TeamsProviders;
use Laravel\Lumen\Application as BaseApplication;
use AlbertCht\Form\FormRequestServiceProvider;

/**
 * Class Application
 * @package App
 */
class Application extends BaseApplication
{
    
    /** @var array */
    private $localServiceProviders = [];
    
    /** @var array */
    private $serviceProviders = [
        FormRequestServiceProvider::class,
        CoreProviders\CoreServiceProvider::class,
        TeamsProviders\TeamsServiceProvider::class,
        TeamsProviders\TeamsRouteServiceProvider::class,
        MatchesProviders\MatchesRouteServiceProvider::class,
        MatchesProviders\MatchesServiceProvider::class
    ];
    
    /**
     * Register the application service providers.
     *
     * @return void
     */
    public function registerServiceProviders(): void
    {
        if (! $this->environment('production')) {
            foreach ($this->localServiceProviders as $serviceProvider) {
                $this->register($serviceProvider);
            }
        }
        
        foreach ($this->serviceProviders as $serviceProvider) {
            $this->register($serviceProvider);
        }
    }
}
