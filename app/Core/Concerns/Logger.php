<?php

declare(strict_types=1);

namespace App\Core\Concerns;

use Illuminate\Container\Container;
use Illuminate\Log\Logger as CoreLogger;

/**
 * Trait Logger
 * @package App\Core\Concerns
 */
trait Logger
{

    /**
     * @return CoreLogger
     */
    public function getLogger(): CoreLogger
    {
        return Container::getInstance()->get(CoreLogger::class);
    }
}
