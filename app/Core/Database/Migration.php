<?php

declare(strict_types=1);

namespace App\Core\Database;

use Illuminate\Database\Migrations\Migration as BaseMigration;

/**
 * Class Migration
 * @package App\Core\Database
 */
abstract class Migration extends BaseMigration
{

    /**
     * @return void
     */
    abstract public function up(): void;

    /**
     * @return void
     */
    abstract public function down(): void;
}
