<?php

declare(strict_types=1);

namespace App\Core\Storage\Entity;

use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Interface OfficialInterface
 * @package App\Core\Storage\Entity
 */
interface OfficialInterface
{

    /**
     * @return HasMany
     */
    public function matchesOfficiated(): HasMany;

    /**
     * @return HasMany
     */
    public function officiatesSports(): HasMany;
}
