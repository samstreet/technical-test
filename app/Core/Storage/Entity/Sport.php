<?php

declare(strict_types=1);

namespace App\Core\Storage\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sport
 * @package App\Core\Storage\Entity
 */
class Sport extends Model implements SportInterface
{

    /**
     * @var string
     */
    protected $table = 'sport';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
