<?php

declare(strict_types=1);

namespace App\Core\Storage\Entity;

use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Player
 * @package App\Core\Storage\Entity
 */
interface PlayerInterface
{

    /**
     * @return HasOne
     */
    public function team(): HasOne;
}
