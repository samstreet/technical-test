<?php

declare(strict_types=1);

namespace App\Core\Storage\Entity;

use App\Teams\Storage\Entity\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Player
 * @package App\Core\Storage\Entity
 */
class Player extends Model implements PlayerInterface
{

    /**
     * @var array
     */
    protected $fillable = [
        'uID',
        'first_name',
        'last_name',
        'position',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'uID' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'position' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @inheritdoc
     */
    public function team(): HasOne
    {
        return $this->hasOne(Team::class);
    }
}
