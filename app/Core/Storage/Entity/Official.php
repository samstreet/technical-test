<?php

declare(strict_types=1);

namespace App\Core\Storage\Entity;

use App\Matches\Storage\Entity\Match;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Official
 * @package App\Core\Storage\Entity
 */
class Official extends Model implements OfficialInterface
{

    /**
     * @var array
     */
    protected $fillable = [
        'uID',
        'first_name',
        'last_name',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'uID' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @inheritDoc
     */
    public function matchesOfficiated(): HasMany
    {
        return $this->hasMany(Match::class);
    }

    /**
     * @inheritDoc
     */
    public function officiatesSports(): HasMany
    {
        return $this->hasMany(Sport::class);
    }
}
