<?php

declare(strict_types=1);


namespace App\Core\Storage\Repository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Core\Storage\Repositories
 */
interface RepositoryInterface
{
    
    /**
     * @return Builder
     */
    public function builder(): Builder;
    
    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes = []): Model;
    
    /**
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool;
    
    /**
     * @param array $records
     *
     * @return void
     */
    public function deleteMany(array $records): void;
    
    /**
     * @param string $value
     *
     * @return bool
     */
    public function existsUsingId(string $value): bool;
    
    /**
     * Load a model using the primary key value.
     *
     * @param string $value
     *
     * @return Model|null
     */
    public function findUsingId(string $value): ?Model;
    
    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getFillableAttributes(array $parameters): array;
    
    /**
     * @param Model $model
     *
     * @return bool
     */
    public function save(Model $model): bool;

    /**
     * @param Model $model
     * @param array $attributes
     * @return bool
     */
    public function update(Model $model, array $attributes): bool;
}
