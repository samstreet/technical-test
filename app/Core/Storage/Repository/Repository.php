<?php

declare(strict_types=1);

namespace App\Core\Storage\Repository;

use App\Core\Storage\Repository\Concerns\HasModel;

/**
 * Class Repository
 * @package App\Core\Storage\Repository
 */
abstract class Repository implements RepositoryInterface
{
    use HasModel;
}
