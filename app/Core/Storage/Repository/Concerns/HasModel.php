<?php

declare(strict_types=1);

namespace App\Core\Storage\Repository\Concerns;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait HasModel
 * @package App\Core\Storage\Repository\Concerns
 */
trait HasModel
{
    
    /**
     * @var Model
     */
    private $entity;
    
    /**
     * @return mixed
     */
    public function getEntity(): Model
    {
        return $this->entity;
    }
    
    /**
     * @param Model $entity
     *
     * @return Model
     */
    public function setEntity(Model $entity): Model
    {
        $this->entity = $entity;
        return $this->entity;
    }
}
