<?php

declare(strict_types=1);

namespace App\Core\Services;

/**
 * Class ParseXMLService
 * @package App\Core\Services
 */
class ParseXMLService implements DataParserInterface
{

    /**
     * @inheritDoc
     */
    public function parse($data): array
    {
        $parsed = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($parsed);
        $parsed = json_decode($json, true);

        return $parsed;
    }
}
