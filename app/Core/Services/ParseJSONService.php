<?php

declare(strict_types=1);

namespace App\Core\Services;

use Illuminate\Support\Collection;

/**
 * Class ParseJSONService
 * @package App\Core\Services
 */
class ParseJSONService implements DataParserInterface
{
    
    /**
     * @inheritDoc
     */
    public function parse($data): array
    {
        return (new Collection()); // TODO - out of current scope
    }
}
