<?php

declare(strict_types=1);

namespace App\Core\Services;

/**
 * Interface DataParserInterface
 * @package App\Core\Services
 */
interface DataParserInterface
{
    
    /**
     * @param $data
     *
     * @return mixed
     */
    public function parse($data): array;
}
