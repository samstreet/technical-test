<?php

declare(strict_types=1);

namespace App\Core\Http\Requests\Concerns;

/**
 * Class RequestHasNoRules
 * @package App\Core\Http\Requests\Concerns
 */
class RequestHasNoRules
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
