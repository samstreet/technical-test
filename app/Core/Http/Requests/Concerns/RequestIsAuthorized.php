<?php

declare(strict_types = 1);

namespace App\Core\Http\Requests\Concerns;

/**
 * Trait RequestIsAuthorized
 * @package App\Core\Http\Requests\Concerns
 */
trait RequestIsAuthorized
{
    
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
