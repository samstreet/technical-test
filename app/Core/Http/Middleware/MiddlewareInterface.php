<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Interface MiddlewareInterface
 * @package App\Core\Http\Middleware
 */
interface MiddlewareInterface
{
    
    /**
     * Handle the request and pass onto the next middleware.
     *
     * @param Request $request
     * @param \Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, \Closure $next): Response;
}
