<?php
declare(strict_types = 1);

namespace App\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 * @package App\Core\Http\Controllers
 */
abstract class Controller extends BaseController
{

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function notFound(string $message): JsonResponse
    {
        return response()->json(
            [
                'errors' => [
                    'Resource Not Available'
                ],
                'message' => $message
            ],
        Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function errored(string $message): JsonResponse
    {
        return response()->json(
            [
                'errors' => [
                    'An error occurred'
                ],
                'message' => $message
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
