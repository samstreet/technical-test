<?php

declare(strict_types=1);

namespace App\Core\Providers;

use App\Core\Providers\Concerns;
use App\Core\Services\DataParserInterface;
use App\Core\Services\ParseJSONService;
use App\Core\Services\ParseXMLService;
use App\Core\Storage\Entity\Player;
use App\Core\Storage\Entity\PlayerInterface;
use App\Core\Storage\Entity\Sport;
use App\Core\Storage\Entity\SportInterface;
use App\Core\Storage\Repository\PlayersRepository;
use App\Core\Storage\Repository\PlayersRepositoryInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class CoreServiceProvider
 * @package App\Core\Providers
 */
class CoreServiceProvider extends ServiceProvider
{
    use Concerns\BootOnlyServiceProvider
        ,Concerns\HasRouter;
    
    /**
     * @var bool
     */
    protected $defer = true;
    
    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            PlayerInterface::class,
            SportInterface::class
        ];
    }
    
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->bootConfigurators();
        $this->registerCoreEntities();
        $this->registerCoreRepositories();
        $this->registerCoreServices();
    }

    /**
     *
     */
    protected function bootConfigurators(): void
    {
        $this->app->configure('app');
        $this->app->configure('database');
    }
    
    /**
     * @return void
     */
    protected function registerCoreEntities(): void
    {
        $this->app->bind(PlayerInterface::class, Player::class);
        $this->app->bind(SportInterface::class, Sport::class);
    }
    
    /**
     * @return void
     */
    protected function registerCoreServices(): void
    {
        $this->app->singleton(DataParserInterface::class, ParseXMLService::class);
        $this->app->singleton(DataParserInterface::class, ParseJSONService::class);
    }
    
    /**
     * @return void
     */
    protected function registerCoreRepositories(): void
    {
        $this->app->singleton(PlayersRepositoryInterface::class, PlayersRepository::class);
    }
}
