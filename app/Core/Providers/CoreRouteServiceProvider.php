<?php

declare(strict_types=1);

namespace App\Core\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class CoreRouteServiceProvider
 * @package App\Core\Providers
 */
abstract class CoreRouteServiceProvider extends ServiceProvider implements RouteServiceProviderInterface
{
    use Concerns\BootOnlyServiceProvider,
        Concerns\HasRouter;
    
    /**
     * @var array
     */
    protected $attributes = [];
    
    /**
     * {@inheritdoc}
     */
    public function boot(): void
    {
        $this->getRouter()->group($this->attributes, $this->getRouteCallback());
    }
}
