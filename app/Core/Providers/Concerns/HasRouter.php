<?php

declare(strict_types=1);

namespace App\Core\Providers\Concerns;

use Illuminate\Container\Container;
use Laravel\Lumen\Routing\Router;

/**
 * Trait HasRouter
 * @package App\Core\ServiceProviders\Concerns
 */
trait HasRouter
{
    
    /**
     * Get the router.
     */
    public function getRouter(): Router
    {
        return Container::getInstance()->make('router');
    }
}
