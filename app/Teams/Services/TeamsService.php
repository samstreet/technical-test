<?php

declare(strict_types=1);

namespace App\Teams\Services;

use App\Teams\Storage\Repository\TeamsRepositoryInterface;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamsService
 * @package App\Teams\Services
 */
class TeamsService implements TeamsServiceInterface
{
    
    /**
     * @var TeamsRepositoryInterface
     */
    protected $teamsRepository;
    
    /**
     * TeamsService constructor.
     *
     * @param TeamsRepositoryInterface $teamsRepository
     */
    public function __construct(TeamsRepositoryInterface $teamsRepository)
    {
        $this->teamsRepository = $teamsRepository;
    }
    
    public function findUsingUId(string $value): ?Model
    {
        $record = null;
        try {
            $record = $this->teamsRepository->findUsingId($value);
        } catch (EntityNotFoundException $exception) {
            // @todo logging
        }
        
        return $record;
    }
    
    /**
     * @inheritDoc
     */
    public function existsUsingUId(string $value): bool
    {
        return $this->teamsRepository->existsUsingId($value);
    }
}
