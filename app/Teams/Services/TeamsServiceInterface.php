<?php

declare(strict_types=1);

namespace App\Teams\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface TeamsServiceInterface
 * @package App\Teams\Services
 */
interface TeamsServiceInterface
{
    
    /**
     * @param string $value
     *
     * @return bool
     */
    public function existsUsingUId(string $value): bool;
    
    /**
     * Load a model using the primary key value.
     *
     * @param string $value
     *
     * @return null|Model
     */
    public function findUsingUId(string $value): ?Model;
}
