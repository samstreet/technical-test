<?php

declare(strict_types=1);

namespace App\Teams\Storage\Entity;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Interface TeamInterface
 * @package App\Teams\Storage\Entity
 */
interface TeamInterface
{
    
    /**
     * @return HasMany
     */
    public function players(): HasMany;
    
    /**
     * @return HasMany
     */
    public function matches(): HasMany;

    /**
     * @return HasOne
     */
    public function sport(): HasOne;
}
