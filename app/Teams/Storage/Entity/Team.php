<?php

declare(strict_types=1);

namespace App\Teams\Storage\Entity;

use App\Core\Storage\Entity\Player;
use App\Core\Storage\Entity\Sport;
use App\Matches\Storage\Entity\Match;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Player
 * @package App\Core\Storage\Entity
 */
class Team extends Model implements TeamInterface
{

    /**
     * @var string
     */
    protected $table = 'team';

    /**
     * @var array
     */
    protected $fillable = [
        'uID',
        'country',
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'uID' => 'string',
        'country' => 'string',
        'name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'uuid'
    ];

    /**
     * @inheritdoc
     */
    public function players(): HasMany
    {
        return $this->hasMany(Player::class);
    }
    
    /**
     * @inheritDoc
     */
    public function matches(): HasMany
    {
        return $this->hasMany(Match::class, 'uuid');
    }

    /**
     * @inheritDoc
     */
    public function sport(): HasOne
    {
        return $this->hasOne(Sport::class);
    }
}
