<?php

declare(strict_types=1);

namespace App\Teams\Storage\Repository;

use App\Core\Storage\Repository\RepositoryInterface;

/**
 * Interface TeamsRepositoryInterface
 * @package App\Teams\Storage\Repository
 */
interface TeamsRepositoryInterface extends RepositoryInterface
{
}
