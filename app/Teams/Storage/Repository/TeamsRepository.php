<?php

declare(strict_types=1);

namespace App\Teams\Storage\Repository;

use App\Core\Storage\Repository\Repository;
use App\Teams\Storage\Entity\TeamInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamsRepository
 * @package App\Teams\Storage\Repository
 */
class TeamsRepository extends Repository implements TeamsRepositoryInterface
{
    
    /**
     * TeamsRepository constructor.
     *
     * @param TeamInterface $team
     */
    public function __construct(TeamInterface $team)
    {
        $this->setEntity($team);
    }
    
    /**
     * @inheritDoc
     */
    public function builder(): Builder
    {
        return $this->getEntity()->newQuery();
    }
    
    /**
     * @inheritDoc
     */
    public function create(array $attributes = []): Model
    {
        return $this->builder()->create($attributes)->getModel();
    }
    
    /**
     * @inheritDoc
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
    
    /**
     * @inheritDoc
     */
    public function deleteMany(array $records): void
    {
        foreach ($records as $record) {
            /** @var Model $record */
            $record->delete();
        }
    }
    
    /**
     * @inheritDoc
     */
    public function existsUsingId(string $value): bool
    {
        return $this->builder()
                    ->where('uId', $value)
                    ->exists();
    }
    
    /**
     * @inheritDoc
     */
    public function findUsingId(string $value): ?Model
    {
        return $this->builder()
                    ->where('uId', $value)
                    ->first();
    }
    
    /**
     * @inheritDoc
     */
    public function getFillableAttributes(array $parameters): array
    {
        return $this->getEntity()->only($parameters);
    }
    
    /**
     * @inheritDoc
     */
    public function save(Model $model): bool
    {
        return $model->save();
    }

    /**
     * @inheritDoc
     */
    public function update(Model $model, array $attributes): bool
    {
        return true;
    }
}
