<?php
/**
 * @author Sam Street
 */

namespace App\Teams\Providers;

use App\Core\Providers;

class TeamsRouteServiceProvider extends Providers\CoreRouteServiceProvider
{
    
    /**
     * {@inheritdoc}
     */
    protected $attributes = [
        'prefix'     => 'teams',
        'namespace'  => 'App\\Teams\\Http\\Controllers',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function getRouteCallback(): \Closure
    {
        return function () {
            $this->getRouter()->get('/', [
                'uses'       => 'TeamsController@index',
                'middleware' => [],
            ]);
        };
    }
}
