<?php

declare(strict_types=1);

namespace App\Teams\Providers;

use App\Core\Providers;
use App\Core\Providers\Concerns;
use App\Teams\Services\TeamsService;
use App\Teams\Services\TeamsServiceInterface;
use App\Teams\Storage\Entity\Team;
use App\Teams\Storage\Entity\TeamInterface;
use App\Teams\Storage\Repository\TeamsRepository;
use App\Teams\Storage\Repository\TeamsRepositoryInterface;

/**
 * Class TeamsServiceProvider
 * @package App\Teams\Providers
 */
class TeamsServiceProvider extends Providers\CoreServiceProvider
{
    use Concerns\BootOnlyServiceProvider;
    
    /**
     * @var bool
     */
    protected $defer = true;
    
    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            TeamInterface::class,
            TeamsRepositoryInterface::class,
            TeamsServiceInterface::class
        ];
    }
    
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->registerTeamEntities();
        $this->registerTeamRepositories();
        $this->registerTeamServices();
    }
    
    /**
     * @return void
     */
    protected function registerTeamEntities(): void
    {
        $this->app->bind(TeamInterface::class, Team::class);
    }
    
    /**
     * @return void
     */
    protected function registerTeamServices(): void
    {
        $this->app->singleton(TeamsServiceInterface::class, TeamsService::class);
    }
    
    /**
     * @return void
     */
    protected function registerTeamRepositories(): void
    {
        $this->app->singleton(TeamsRepositoryInterface::class, TeamsRepository::class);
    }
}
