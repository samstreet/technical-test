<?php

declare(strict_types=1);

namespace App\Teams\Http\Controllers;

use App\Core\Http\Controllers\Controller as CoreController;
use App\Teams\Services\TeamsServiceInterface;
use Illuminate\Http\JsonResponse;

/**
 * Class TeamsController
 * @package  App\Teams\Http\Controllers
 */
class TeamsController extends CoreController
{
    
    /**
     * @var TeamsServiceInterface
     */
    private $teamsService;
    
    /**
     * TeamsController constructor.
     *
     * @param TeamsServiceInterface $teamsService
     */
    public function __construct(TeamsServiceInterface $teamsService)
    {
        $this->teamsService = $teamsService;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(["hello" => "world"]);
    }
}
