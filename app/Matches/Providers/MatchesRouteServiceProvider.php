<?php

declare(strict_types=1);

namespace App\Matches\Providers;

use App\Core\Providers;

class MatchesRouteServiceProvider extends Providers\CoreRouteServiceProvider
{
    
    /**
     * {@inheritdoc}
     */
    protected $attributes = [
        'prefix'     => 'matches',
        'namespace'  => 'App\\Matches\\Http\\Controllers',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function getRouteCallback(): \Closure
    {
        return function () {
            $this->getRouter()->get('/', [
                'uses'       => 'MatchesController@index',
                'middleware' => [],
            ]);

            /**
             * Current scope for the project is to _only_ deal with football
             * in most other situations I would use {sport}
             */
            $this->getRouter()->post('/football/{matchId}', [
                'uses'       => 'MatchesController@matchData',
                'middleware' => [],
            ]);

            $this->getRouter()->get('/football/{matchId}', [
                'uses'       => 'MatchesController@getMatchDataForMatch',
                'middleware' => [],
            ]);
        };
    }
}
