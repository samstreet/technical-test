<?php

declare(strict_types=1);

namespace App\Matches\Providers;

use App\Core\Providers;
use App\Core\Providers\Concerns;
use App\Core\Services\DataParserInterface;
use App\Core\Services\ParseXMLService;
use App\Matches\Services\MatchDataService;
use App\Matches\Services\MatchDataServiceInterface;
use App\Matches\Storage\Entity\Match;
use App\Matches\Storage\Entity\MatchInterface;
use App\Matches\Storage\Repository\MatchesRepository;
use App\Teams\Storage\Repository\MatchesRepositoryInterface;

/**
 * Class TeamsServiceProvider
 * @package App\Teams\Providers
 */
class MatchesServiceProvider extends Providers\CoreServiceProvider
{
    use Concerns\BootOnlyServiceProvider;
    
    /**
     * @var bool
     */
    protected $defer = true;
    
    /**
     * @inheritdoc
     */
    public function boot(): void
    {
        $this->registerMatchesEntities();
        $this->registerMatchesRepositories();
        $this->registerMatchesServices();
        $this->registerMatchesResolvers();
    }
    
    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            MatchDataServiceInterface::class,
            MatchesRepositoryInterface::class,
            MatchInterface::class
        ];
    }
    
    /**
     * @return void
     */
    protected function registerMatchesResolvers(): void
    {
        $this->app->when(MatchDataService::class)
                  ->needs(DataParserInterface::class)
                  ->give(ParseXMLService::class);
    }

    /**
     * @return void
     */
    protected function registerMatchesEntities(): void
    {
        $this->app->singleton(MatchInterface::class, Match::class);
    }

    /**
     * @return void
     */
    protected function registerMatchesRepositories(): void
    {
        $this->app->singleton(MatchesRepositoryInterface::class, MatchesRepository::class);
    }

    /**
     * @return void
     */
    protected function registerMatchesServices(): void
    {
        $this->app->singleton(MatchDataServiceInterface::class, MatchDataService::class);
    }
}
