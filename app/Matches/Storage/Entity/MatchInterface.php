<?php

declare(strict_types=1);

namespace App\Matches\Storage\Entity;

use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Interface MatchInterface
 * @package App\Matches\Storage\Entity
 */
interface MatchInterface
{

    /**
     * @return HasOne
     */
    public function homeTeam(): HasOne;

    /**
     * @return HasOne
     */
    public function awayTeam(): HasOne;

    /**
     * @return HasOne
     */
    public function sport(): HasOne;
}
