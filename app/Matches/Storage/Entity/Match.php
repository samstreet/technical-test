<?php

declare(strict_types=1);

namespace App\Matches\Storage\Entity;

use App\Core\Storage\Entity\Sport;
use App\Teams\Storage\Entity\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Match
 * @package App\Teams\Storage\Entity
 */
class Match extends Model implements MatchInterface
{

    /**
     * @var string
     */
    protected $table = 'match';

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'sport_uuid',
        'home_team',
        'away_team',
        'value',
        'duration',
        'started_at',
        'ended_at',
        'created_at',
        'updated_at',
        'match_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'duration' => 'integer',
        'started_at' => 'datetime',
        'ended_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * @inheritdoc
     */
    public function homeTeam(): HasOne
    {
        return $this->hasOne(Team::class, 'uuid', 'home_team');
    }

    /**
     * @inheritdoc
     */
    public function awayTeam(): HasOne
    {
        return $this->hasOne(Team::class, 'uuid', 'away_team');
    }

    /**
     * @inheritdoc
     */
    public function sport(): HasOne
    {
        return $this->hasOne(Sport::class, 'uuid', 'sport_uuid');
    }
}
