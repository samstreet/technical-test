<?php

declare(strict_types=1);

namespace App\Matches\Storage\Repository;

use App\Core\Storage\Repository\Repository;
use App\Matches\Storage\Entity\MatchInterface;
use App\Teams\Storage\Repository\MatchesRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MatchesRepository
 * @package App\Teams\Storage\Repository
 */
class MatchesRepository extends Repository implements MatchesRepositoryInterface
{
    
    /**
     * MatchesRepository constructor.
     *
     * @param MatchInterface $match
     */
    public function __construct(MatchInterface $match)
    {
        $this->setEntity($match);
    }
    
    /**
     * @inheritDoc
     */
    public function builder(): Builder
    {
        return $this->getEntity()->newQuery();
    }
    
    /**
     * @inheritDoc
     */
    public function create(array $attributes = []): Model
    {
        return $this->builder()->create($attributes)->getModel();
    }
    
    /**
     * @inheritDoc
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
    
    /**
     * @inheritDoc
     */
    public function deleteMany(array $records): void
    {
        foreach ($records as $record) {
            /** @var Model $record */
            $record->delete();
        }
    }
    
    /**
     * @inheritDoc
     */
    public function existsUsingId(string $value): bool
    {
        return $this->builder()
                    ->where('match_id', $value)
                    ->exists();
    }
    
    /**
     * @inheritDoc
     */
    public function findUsingId(string $value): ?Model
    {
        return $this->builder()
                    ->where('match_id', $value)
                    ->first();
    }
    
    /**
     * @inheritDoc
     */
    public function getFillableAttributes(array $parameters): array
    {
        return $this->getEntity()->only($parameters);
    }
    
    /**
     * @inheritDoc
     */
    public function save(Model $model): bool
    {
        return $model->save();
    }

    /**
     * @inheritDoc
     */
    public function update(Model $model, array $attributes): bool
    {
        $match = $model->fill($attributes);
        return $this->save($match);
    }
}
