<?php

declare(strict_types=1);

namespace App\Matches\Http\Requests;

use AlbertCht\Form\FormRequest;
use App\Core\Http\Requests\CoreRequest;
use App\Matches\Services\MatchDataServiceInterface;
use Illuminate\Validation\ValidationException;

/**
 * Class GetMatchDataForMatchRequest
 * @package App\Matches\Http\Requests
 */
class GetMatchDataForMatchRequest extends CoreRequest
{

    /**
     * @var MatchDataServiceInterface
     */
    private $matchDataService;

    /**
     * GetMatchDataForMatchRequest constructor.
     * @param MatchDataServiceInterface $matchDataService
     */
    public function __construct(MatchDataServiceInterface $matchDataService)
    {
        $this->matchDataService = $matchDataService;
    }

    /**
     * @var array
     */
    protected $acceptableContentTypes = [
        'application/json'
    ];

    public function validator()
    {
        dd('ddfdf');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'matchId' => 'unique:match,match_id'
        ];
    }
}
