<?php

declare(strict_types=1);

namespace App\Matches\Http\Requests;

use App\Core\Http\Requests\CoreRequest;

/**
 * Class MatchDataRequest
 * @package App\Matches\Http\Requests
 */
class MatchDataRequest extends CoreRequest
{

    /**
     * @var array
     */
    protected $acceptableContentTypes = [
        'application/json'
    ];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "match_id" => "integer|required",
            "feed_file" => "string|required",
        ];
    }
}
