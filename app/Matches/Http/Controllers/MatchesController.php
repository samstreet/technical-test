<?php

declare(strict_types=1);

namespace App\Matches\Http\Controllers;

use App\Core\Http\Controllers\Controller as CoreController;
use App\Matches\Http\Requests\GetMatchDataForMatchRequest;
use App\Matches\Http\Requests\MatchDataRequest;
use App\Matches\Services\MatchDataServiceInterface;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TeamsController
 * @package  App\Teams\Http\Controllers
 */
class MatchesController extends CoreController
{

    /**
     * @var MatchDataServiceInterface
     */
    protected $matchDataService;

    /**
     * MatchesController constructor.
     *
     * @param MatchDataServiceInterface $matchDataService
     */
    public function __construct(MatchDataServiceInterface $matchDataService)
    {
        $this->matchDataService = $matchDataService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(["matches" => "index"]);
    }

    /**
     * @param MatchDataRequest $request
     * @return JsonResponse
     */
    public function matchData(MatchDataRequest $request): JsonResponse
    {
        $feed = $this->matchDataService->fetchDataFromSource($request->get('feed_file'));
        if (empty($feed)) {
            return $this->notFound('Unable to retrieve data from source');
        }

        $persisted = $this->matchDataService->persistMatchDataForMatch($request->get('match_id'), $feed);
        if (!$persisted) {
            return $this->errored('An Error Occurred Persisting Data');
        }

        return response()->json([], Response::HTTP_ACCEPTED);
    }

    /**
     * @param GetMatchDataForMatchRequest $request
     * @return JsonResponse
     */
    public function getMatchDataForMatch(GetMatchDataForMatchRequest $request): JsonResponse
    {
        ;
        return response()->json();
    }
}
