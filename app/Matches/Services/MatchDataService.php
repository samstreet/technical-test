<?php

declare(strict_types=1);

namespace App\Matches\Services;

use App\Core\Concerns\Logger;
use App\Core\Services\DataParserInterface;
use App\Matches\Storage\Entity\MatchInterface;
use App\Teams\Services\TeamsServiceInterface;
use App\Teams\Storage\Repository\MatchesRepositoryInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class MatchDataService
 * @package App\Matches\Services
 */
class MatchDataService implements MatchDataServiceInterface
{
    use Logger;

    /**
     * @var DataParserInterface
     */
    protected $dataParser;

    /**
     * @var MatchesRepositoryInterface
     */
    protected $matchesRepository;

    /**
     * @var TeamsServiceInterface
     */
    protected $teamService;

    /**
     * MatchDataService constructor.
     * @param DataParserInterface $dataParser
     * @param MatchesRepositoryInterface $matchesRepository
     * @param TeamsServiceInterface $teamsService
     */
    public function __construct(
        DataParserInterface $dataParser,
        MatchesRepositoryInterface $matchesRepository,
        TeamsServiceInterface $teamsService
    ) {
        $this->dataParser = $dataParser;
        $this->matchesRepository = $matchesRepository;
        $this->teamService = $teamsService;
    }

    /**
     * @inheritDoc
     */
    public function fetchDataFromSource(string $source): ?array
    {
        try {
            $feed = file_get_contents($source);
            if (empty($feed)) {
                throw new \Exception('Unable to retrieve match data from source');
            }

            return $this->dataParser->parse($feed);
        } catch (\Exception $exception) {
            $this->getLogger()->error($exception->getMessage(), compact('exception'));
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function extractDataFromSource(array $data, string $needle): array
    {
        $results = [];

        if (isset($data[$needle])) {
            $results[] = $data;
        }

        foreach ($data as $subarray) {
            if (is_array($subarray)) {
                $results = array_merge($results, $this->extractDataFromSource($subarray, $needle));
            }
        }

        return $results;
    }

    /**
     * @inheritDoc
     */
    public function persistMatchDataForMatch(int $matchId, array $matchData): bool
    {
        $match = $this->matchesRepository->findUsingId((string)$matchId);

        if (!$match) {
            try {
                $match = $this->createMatchDataRecord($matchId, $matchData);
            } catch (\Exception $exception) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function fetchMatchDataForMatch(int $matchId): ?MatchInterface
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function doesMatchExist(int $match): bool
    {
        return $this->matchesRepository->existsUsingId((string)$match);
    }


    /**
     * @param int $match
     * @param array $matchData
     * @return MatchInterface|null
     */
    protected function createMatchDataRecord(int $match, array $matchData): ?MatchInterface
    {
        try {
            $teams = [];
            $t = (object) $matchData['SoccerDocument'];
            foreach ($t->MatchData['TeamData'] as $teamData) {
                $teams[strtolower($teamData['@attributes']["Side"])] = $teamData['@attributes']["TeamRef"];
            };

            $data = [
                'uuid' => Uuid::uuid4(),
                'sport_uuid' => '4bfc9c1c-99f3-4372-a644-0bd3118034e1', // todo deal with a service,
                'home_team' => $this->teamService->findUsingUId($teams['home'])->uuid,
                'away_team' => $this->teamService->findUsingUId($teams['away'])->uuid,
                'match_id' => $match
            ];

            $match = $this->matchesRepository->create($data);

            return $match;
        } catch (\Exception $exception) {
            dd($exception);
            $this->getLogger()->error($exception->getMessage(), compact('exception'));
        }

        return null;
    }
}
