<?php

declare(strict_types=1);

namespace App\Matches\Services;

use App\Matches\Storage\Entity\MatchInterface;

/**
 * Interface MatchDataServiceInterface
 * @package App\Matches\Services
 */
interface MatchDataServiceInterface
{

    /**
     * @param string $source
     * @return array
     */
    public function fetchDataFromSource(string $source): ?array;

    /**
     * @param array $source
     * @param string $key
     * @return array
     */
    public function extractDataFromSource(array $source, string $key): array;

    /**
     * @param int $match
     * @param array $matchData
     * @return mixed
     */
    public function persistMatchDataForMatch(int $match, array $matchData): bool;

    /**
     * @param int $matchId
     * @return MatchInterface|null
     */
    public function fetchMatchDataForMatch(int $matchId): ?MatchInterface;

    /**
     * @param int $match
     * @return bool
     */
    public function doesMatchExist(int $match): bool;
}
