# Technical Test
#### Author: Sam Street

#### Setup
- `cp .env.example .env` I understand that it is bad practise to include the actual env variables in the `.env.example` but for this demonstration it was easier to set up this way.
- Serve via ```php -S localhost:8000 -t public  ``` from the project root
- `docker-compose up -d` to set up the database container
- Run the migrations: ```php artisan migrate --seed```
- Tests: `./vendor/bin/phpunit --testsuite=Unit`

#### Technical specification
- Framework: Lumen using Eloquent
- Database: Docker container running MySql 5.7 
- Utilising Service Orientated Architecture (SOA)