<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

$app = new App\Application(realpath(__DIR__ . '/../'));

$app->withEloquent();
$app->singleton(Illuminate\Contracts\Debug\ExceptionHandler::class, App\Core\Exceptions\Handler::class);
$app->singleton(Illuminate\Contracts\Console\Kernel::class, App\Core\Console\Kernel::class);
$app->registerServiceProviders();

return $app;