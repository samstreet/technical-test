<?php

declare(strict_types=1);

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * Class sport
 */
class SportSeeder extends Seeder
{

    /**
     * @return void
     */
    public function run(): void
    {
        DB::table('sport')->insert([
            'uuid' => '4bfc9c1c-99f3-4372-a644-0bd3118034e1',
            'name' => 'football'
        ]);
    }
}
