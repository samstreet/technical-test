<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Database\Seeds;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Seeds\SportSeeder::class);
        $this->call(Seeds\TeamSeeder::class);
    }
}