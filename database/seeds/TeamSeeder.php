<?php

declare(strict_types=1);

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * Class sport
 */
class TeamSeeder extends Seeder
{

    /**
     * @return void
     */
    public function run(): void
    {
        DB::table('team')->insert([
            'uuid' => Uuid::uuid4(),
            'country' => 'England',
            'name' => 'Arsenal',
            'uID' => 't3',
            'sport_uuid' => '4bfc9c1c-99f3-4372-a644-0bd3118034e1'
        ]);

        DB::table('team')->insert([
            'uuid' => Uuid::uuid4(),
            'country' => 'England',
            'name' => 'Leicester City',
            'uID' => 't13',
            'sport_uuid' => '4bfc9c1c-99f3-4372-a644-0bd3118034e1'
        ]);
    }
}
