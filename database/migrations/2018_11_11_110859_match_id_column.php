<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class MatchIdColumn
 */
class MatchIdColumn extends \App\Core\Database\Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::table('match', function(Blueprint $table){
            $table->string('match_id', 50)->unique();
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::table('match', function(Blueprint $table){
            $table->dropColumn(['match_id']);
        });
    }

}
