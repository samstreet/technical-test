<?php

declare(strict_types=1);

use App\Core\Database\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class PositionTable
 */
class PositionTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('position', function (Blueprint $table){
            $table->string('uuid', 50)->primary();
            $table->string('sport_uuid', 50);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('sport_uuid')->references('uuid')->on('sport');
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::table('position', function(Blueprint $table)
        {
            $table->dropForeign('position_sport_uuid_foreign');
            $table->drop();
        });
    }

}
