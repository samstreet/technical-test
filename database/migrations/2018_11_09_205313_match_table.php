<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Core\Database\Migration;

/**
 * Class MatchTable
 */
class MatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('match', function (Blueprint $table){
            $table->string('uuid', 50)->primary();
            $table->string('sport_uuid', 50);
            $table->string('home_team', 50);
            $table->string('away_team', 50);
            $table->integer('value')->nullable();
            $table->integer('duration')->default(0);
            $table->dateTime('started_at')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('sport_uuid')->references('uuid')->on('sport');
            $table->foreign('home_team')->references('uuid')->on('team');
            $table->foreign('away_team')->references('uuid')->on('team');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('match', function(Blueprint $table)
        {
            $table->dropForeign('match_sport_uuid_foreign');
            $table->dropForeign('match_home_team_foreign');
            $table->dropForeign('match_away_team_foreign');
            $table->drop();
        });
    }
}
