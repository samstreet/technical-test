<?php

declare(strict_types=1);

use App\Core\Database\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class PlayersTable
 */
class PlayersTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('player', function(Blueprint $table){
            $table->string('uuid', 50)->primary();
            $table->string('uID', 50)->unique();
            $table->string('first_name', 60);
            $table->string('last_name', 60);
            $table->integer('team_id');
            $table->integer('position_id');
            $table->integer('sport_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Illuminate\Support\Facades\Schema::drop('player');
    }
}
