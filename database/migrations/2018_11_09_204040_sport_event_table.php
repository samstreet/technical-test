<?php

declare(strict_types=1);

use App\Core\Database\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class SportEventTable
 */
class SportEventTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('sport_event', function (Blueprint $table){
            $table->string('uuid', 50)->primary();
            $table->string('type', 50);
            $table->integer('value');
            $table->dateTime('happened_at');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::drop('sport_event');
    }

}
