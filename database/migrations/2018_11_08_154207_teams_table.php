<?php

declare(strict_types=1);

use App\Core\Database\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

/**
 * Class TeamsTable
 */
class TeamsTable extends Migration
{

    /**
     * @return void
     */
    public function up(): void
    {
        Schema::create('team', function(Blueprint $table){
            $table->string('uuid', 50)->primary();
            $table->string('uID', 50)->unique();
            $table->string('sport_uuid', 50);
            $table->string('country', 60);
            $table->string('name', 60);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('team');
    }
}
