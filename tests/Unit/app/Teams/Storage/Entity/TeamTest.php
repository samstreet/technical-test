<?php

declare(strict_types=1);

namespace Tests\Unit\App\Teams\Storage\Entity;

use App\Teams\Storage\Entity\Team;
use App\Teams\Storage\Entity\TeamInterface;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

/**
 * Class TestTeam
 * @package Test\App\Teams\Storage\Entity
 */
class TeamTest extends TestCase
{
    
    /**
     * @var Team
     */
    private $team;
    
    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->team = new Team();
    }
    
    public function testTeamUtilisesCorrectInterface()
    {
        $this->assertInstanceOf(TeamInterface::class, $this->team);
    }
    
    public function testPlayersIsCorrectRelationshipType()
    {
        $this->assertInstanceOf(HasMany::class, $this->team->players());
    }
    
    public function testMatchesIsCorrectRelationshipType()
    {
        $this->assertInstanceOf(HasMany::class, $this->team->matches());
    }
}
