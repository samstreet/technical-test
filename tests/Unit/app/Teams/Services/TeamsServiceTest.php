<?php

declare(strict_types=1);

namespace Tests\Unit\App\Teams\Services;

use App\Teams\Services\TeamsService;
use App\Teams\Storage\Entity\Team;
use App\Teams\Storage\Repository\TeamsRepository;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Tests\TestCase;

/**
 * Class TeamsServiceTest
 * @package Tests\Unit\App\Teams\Services
 */
class TeamsServiceTest extends TestCase
{

    /**
     * We have a model by that ID, it should be returned correctly
     */
    public function testServiceFindUsingIdReturnsTeamWhenEntityFound()
    {
        $repository = $this->getMockBuilder(TeamsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingId'])
            ->getMock();

        $repository->expects($this->once())
            ->method('findUsingId')
            ->with('1')
            ->willReturn($team = new Team());

        $service = new TeamsService($repository);
        $this->assertSame($team, $service->findUsingUId('1'));
    }

    /**
     * We don't have an entity by that ID, it should be null
     */
    public function testServiceFindUsingIdReturnsNullWhenEntityNotFound()
    {
        $repository = $this->getMockBuilder(TeamsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingId'])
            ->getMock();

        $repository->expects($this->once())
            ->method('findUsingId')
            ->with('1')
            ->willThrowException(new EntityNotFoundException("Team", "1"));

        $service = new TeamsService($repository);
        $this->assertNull($service->findUsingUId('1'));
    }

    /**
     * We don't have an entity by that ID, it should be false
     */
    public function testServiceExistsUsingIdReturnsNullWhenDoesNotExist()
    {
        $repository = $this->getMockBuilder(TeamsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['existsUsingId'])
            ->getMock();

        $repository->expects($this->once())
            ->method('existsUsingId')
            ->with('1')
            ->willReturn(false);

        $service = new TeamsService($repository);
        $this->assertFalse($service->existsUsingUId('1'));
    }

    /**
     * We don't have an entity by that ID, it should be false
     */
    public function testServiceExistsUsingIdReturnsTrueWhenDoesExist()
    {
        $repository = $this->getMockBuilder(TeamsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['existsUsingId'])
            ->getMock();

        $repository->expects($this->once())
            ->method('existsUsingId')
            ->with('1')
            ->willReturn(true);

        $service = new TeamsService($repository);
        $this->assertTrue($service->existsUsingUId('1'));
    }
}
