<?php

declare(strict_types=1);

namespace Tests\Unit\App\Matches\Services;

use App\Core\Services\ParseXMLService;
use App\Matches\Services\MatchDataService;
use App\Matches\Storage\Entity\Match;
use App\Matches\Storage\Repository\MatchesRepository;
use App\Teams\Services\TeamsService;
use App\Teams\Storage\Entity\Team;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

/**
 * Class MatchDataServiceTest
 * @package Tests\Unit\App\Matches\Services
 */
class MatchDataServiceTest extends TestCase
{

    /**
     * Test the fetchDataFromSource will return null when file is not found
     * @covers MatchDataService::fetchDataFromSource()
     */
    public function testFetchDataFromSourceReturnsNullWhenNoFileFound()
    {
        $repository = $this->getMockBuilder(MatchesRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingUId'])
            ->getMock();

        $dataParser = new ParseXMLService();

        $teamService = $this->getMockBuilder(TeamsService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new MatchDataService($dataParser, $repository, $teamService);
        $this->assertNull($service->fetchDataFromSource(base_path('')));
    }

    /**
     * Test the fetchDataFromSource will return null when file is not found
     * @covers MatchDataService::fetchDataFromSource()
     */
    public function testFetchDataFromSourceReturnsDomDocumentWhenFileExists()
    {
        $repository = $this->getMockBuilder(MatchesRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingUId'])
            ->getMock();

        $dataParser = new ParseXMLService();

        $teamService = $this->getMockBuilder(TeamsService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new MatchDataService($dataParser, $repository, $teamService);
        $this->assertTrue(is_array($service->fetchDataFromSource(base_path('feeds/sports/football/match-2723/20170811_184516.xml'))));
    }

    /**
     * Test extractDataFromSource
     * @covers MatchDataService::extractDataFromSource()
     */
    public function testExtractDataFromSource()
    {
        $repository = $this->getMockBuilder(MatchesRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingUId'])
            ->getMock();

        $dataParser = new ParseXMLService();

        $teamService = $this->getMockBuilder(TeamsService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new MatchDataService($dataParser, $repository, $teamService);
        $feed = $service->fetchDataFromSource(base_path('feeds/sports/football/match-2723/20170811_184516.xml'));
        $this->assertTrue(is_array($service->extractDataFromSource($feed, 'MatchOfficial')));
    }

    /**
     * Test extractDataFromSource
     * @covers MatchDataService::persistMatchDataForMatch()
     */
    public function testPersistMatchDataForMatchReturnsModel()
    {
        $repository = $this->getMockBuilder(MatchesRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingId', 'create', 'update'])
            ->getMock();

        $match = new Match();

        $teamService = $this->getMockBuilder(TeamsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUsingUId'])
            ->getMock();

        $teamMock = $this->getMockBuilder(Team::class)
            ->disableOriginalConstructor()
            ->getMock();

        $teamMock2 = clone $teamMock;

        $teamMock->uuid = Uuid::uuid4();
        $teamMock2->uuid = Uuid::uuid4();

        $teamService->method('findUsingUId')->willReturn($teamMock, $teamMock2);

        $repository->expects($this->once())->method('findUsingId')->willReturn(null);
        $repository->expects($this->once())->method('create')->willReturn($match);

        $dataParser = new ParseXMLService();

        $service = new MatchDataService($dataParser, $repository, $teamService);
        $feed = $service->fetchDataFromSource(base_path('feeds/sports/football/match-2723/20170811_184516.xml'));
        $this->assertTrue($service->persistMatchDataForMatch(123, $feed));
    }
}
